/**
 * Created by jason on 4/29/16.
 */
server = "http://localhost:3000/"

function loadEditor() {
    var editor;
    editor = ContentTools.EditorApp.get();
    editor.init('*[data-editable]', 'data-name');

    editor.addEventListener('saved', function(ev) {
        var name, payload, regions, xhr;
        regions = ev.detail().regions;
        if (Object.keys(regions).length == 0) {
            return;
        }

        this.busy(true);

        updates = {};

        for (name in regions) {
            if (regions.hasOwnProperty(name)) {
                updates[name] = new HTMLString.String(regions[name]).unformat(0, -1,
                new HTMLString.Tag("p"), new HTMLString.Tag("h1")).html();
                console.log(updates[name])
            }
        }

        $.ajax({
            method: "PATCH",
            data: updates,
            url: server + "page/" + $('body').data().id,
            error: function() {
                console.log("AJAX request failed to send");
            },
            success: function() {
                console.log("AJAX request sent");
                if ("title" in updates) {
                    $('title').html(updates["title"]);
                }
            }
        });
        this.busy(false);
    })
}

$(document).ready(function() {
    $(window).on('load', function() {
        loadEditor();
    });
});