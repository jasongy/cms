var mongoose = require('mongoose');
// Move to models folder and then get it from there, separate it from routes
var pageSchema = mongoose.Schema({
    title: String,
    body: String,
    url: String
});

module.exports = mongoose.model("cms", pageSchema);
