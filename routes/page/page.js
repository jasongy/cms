var express = require('express');
var router = express.Router();

var cors = require('cors');

var Page = require("../../models/pageSchema.js");


router.get('/:url', function(req, res, next) {
	var query = Page.where({url: req.params.url});
	query.findOne(function (err, page) {
		if (err) throw err;
		console.log(page);
		res.render('page/showPage', {page})
	})
});

router.patch("/:id", cors(), function(req, res) {
	Page.findByIdAndUpdate(req.params.id, req.body, function(err) {
		if (err) throw err;
		res.send("UPDATED");
	});
});

module.exports = router;
