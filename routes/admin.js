/**
 * Created by jason on 4/27/16.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var cors = require('cors');

var Page = require('../models/pageSchema.js');

router.get('/', function(req, res, next) {
    Page.find(function (err, pages) {
        if (err) return console.error(err);
        res.render('admin', { title: 'Pages', pages: pages });
    });    
});

router.get('/del/:id', function(req, res, next) {
    Page.findByIdAndRemove(req.params.id, function(err, page) {
        if (err) throw err;
        res.redirect('/admin');
    });
});

router.get('/newPage', function(req, res) {
    var page = {
        title: "",
        body: "",
        url: "",
        id: ""
    };
    res.render("admin/pageForm", {title: "Add new page", page});
});

router.post('/save', function(req, res) {
    var id = req.body.id;
    if (id === "") {
        var newPage = new Page(req.body);
        newPage.save(function(err) {
            if (err) throw err;
            res.redirect('/admin');
        })
    } else {
        Page.update({_id: id}, req.body, function (err) {
            if (err) throw err;
            res.redirect('/admin');
        });
    }
});

router.get('/update/:id', function(req, res) {
    Page.findById(req.params.id, function(err, page) {
        if (err) throw err;
        res.render('admin/pageForm', {title: "Update: " + page.title, page});
    });
});

module.exports = router;
